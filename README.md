# OpenML dataset: cloud-dataset

https://www.openml.org/d/43553

## Structure

The dataset has the following file structure:

* `dataset/`
  * `tables/`
    * [`data.pq`](./dataset/tables/data.pq): Parquet file with data
  * [`metadata.json`](./dataset/metadata.json): OpenML description of the dataset
  * [`features.json`](./dataset/features.json): OpenML description of table columns
  * [`qualities.json`](./dataset/qualities.json): OpenML qualities (meta-features)

## Description

Data from StatLib (ftp stat.cmu.edu/datasets)
These data are those collected in a cloud-seeding experiment in Tasmania between mid-1964 and January 1971. Their analysis, using regression techniques and permutation tests, is discussed in:
Miller, A.J., Shaw, D.E., Veitch, L.G.  Smith, E.J. (1979).
   Analyzing the results of a cloud-seeding experiment in Tasmania',
   Communications in Statistics - Theory  Methods, vol.A8(10),
   1017-1047.
The rainfalls are period rainfalls in inches. TE and TW are the east and west target areas respectively, while NC, SC and NWC are the corresponding rainfalls in the north, south and north-west control areas respectively. S = seeded, U = unseeded.
Rain in eastern target region is being treated as the class attribute. (Attribute for rain in the western target region has been deleted.)

## Contributing

This is a [read-only mirror](https://gitlab.com/data/d/openml/43553) of an [OpenML dataset](https://www.openml.org/d/43553). Contribute any changes to the dataset there. Alternatively, [fork the dataset](https://gitlab.com/data/d/openml/43553/-/forks/new) or [find an existing fork](https://gitlab.com/data/d/openml/43553/-/forks) to contribute to.

You can use [issues](https://gitlab.com/data/d/openml/43553/-/issues) to discuss the dataset and any issues.

For more information see [https://datagit.org/](https://datagit.org/).

